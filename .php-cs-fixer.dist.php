<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__ . '/src')
;

return SlyFoxCreative\CodingStyles\config($finder);
