<?php

declare(strict_types=1);

namespace SlyFoxCreative\CodingStyles;

use PhpCsFixer\Config;
use PhpCsFixer\ConfigInterface;
use PhpCsFixer\Finder;

/** @param array<string, array<string, mixed>|bool> $rules */
function config(Finder $finder, array $rules = []): ConfigInterface
{
    $defaultRules = require __DIR__ . '/rules.php';
    $rules = array_merge($defaultRules, $rules);

    return (new Config())
        ->setFinder($finder)
        ->setRiskyAllowed(true)
        ->setRules($rules)
    ;
}
