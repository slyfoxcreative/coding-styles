<?php

return [
    '@PHP80Migration' => PHP_MAJOR_VERSION === 8 && PHP_MINOR_VERSION === 0,
    '@PHP81Migration' => PHP_MAJOR_VERSION === 8 && PHP_MINOR_VERSION === 1,
    '@PHP82Migration' => PHP_MAJOR_VERSION === 8 && PHP_MINOR_VERSION === 2,
    '@PhpCsFixer' => true,
    'concat_space' => [
        'spacing' => 'one',
    ],
    'mb_str_functions' => true,
    'ordered_imports' => [
        'imports_order' => [
            'const',
            'class',
            'function',
        ],
    ],
    'phpdoc_add_missing_param_annotation' => false,
    'php_unit_internal_class' => false,
    'php_unit_test_class_requires_covers' => false,
    'yoda_style' => false,
];
