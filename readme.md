# Coding Style Configuration Files

## PHP

* slyfoxcreative/coding-styles

## JavaScript

* babel-eslint
* eslint
* eslint-config-prettier
* eslint-plugin-prettier
* prettier

## CSS and SCSS

* prettier
* stylelint
* stylelint-config-prettier
* stylelint-config-standard
* stylelint-order
